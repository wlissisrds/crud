import React, { useState } from "react";

const Pesquisar = () => {

    const [getInput, setInput] = useState();

    const valorInput = (e) => {
        setInput(e.target.value);
        e.preventDefault();
    }



    return (
        <div>Pesquisar
            <div>
                <button><a href={`http://www.${getInput}.com`}>Pesquisar</a></button>
            </div>

            <input type="text" value={getInput} onChange={valorInput} />
        </div>
    )
}

export default Pesquisar;